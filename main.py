from __future__ import annotations

from pathlib import Path
from typing import Callable

import cv2 as cv
import easygui
import numpy as np
import pymsgbox


def shrink_to_fit(image: np.ndarray, max_size: tuple[int, int]) -> np.ndarray:
    target_width, target_height = max_size
    assert target_width > 0
    assert target_height > 0

    assert target_width >= target_height
    height, width = image.shape[0], image.shape[1]

    if width >= target_width:
        width, height = target_width, height * target_width // width
        image = cv.resize(image, (width, height))

    height, width = image.shape[0], image.shape[1]

    if height >= target_height:
        width, height = width * target_height // height, target_height
        image = cv.resize(image, (width, height))

    return image


class Model:
    def __init__(
            self,
            image: np.ndarray,
            on_parameter_changed: Callable[[np.ndarray, np.ndarray], None]
    ) -> None:
        self._image = image
        self._on_parameter_changed = on_parameter_changed

        self.gauss_ksize = 3

        self.canny_threshold1 = 100
        self.canny_threshold2 = 200

        self.rho = 1
        self.theta_degrees = 1
        self.threshold = 50
        self.min_line_length = 50
        self.max_line_gap = 50

    def set_gauss_ksize(self, value: int) -> None:
        self.gauss_ksize = max(value, 1)
        if self.gauss_ksize % 2 == 0:
            self.gauss_ksize += 1
        self._parameter_changed()

    def set_canny_threshold1(self, value: int) -> None:
        self.canny_threshold1 = value
        self._parameter_changed()

    def set_canny_threshold2(self, value: int) -> None:
        self.canny_threshold2 = value
        self._parameter_changed()

    def set_rho(self, value: int) -> None:
        self.rho = max(value, 1)
        self._parameter_changed()

    def set_theta(self, value: int) -> None:
        self.theta_degrees = max(value, 1)
        self._parameter_changed()

    def set_threshold(self, value: int) -> None:
        self.threshold = value
        self._parameter_changed()

    def set_min_line_length(self, value: int) -> None:
        self.min_line_length = value
        self._parameter_changed()

    def set_max_line_gap(self, value: int) -> None:
        self.max_line_gap = value
        self._parameter_changed()

    def _hough_lines_p(self) -> tuple[np.ndarray, np.ndarray]:
        edges = self._canny()
        return edges, cv.HoughLinesP(
            edges,
            rho=self.rho, theta=self.theta_degrees / 180 * np.pi,
            threshold=self.threshold,
            maxLineGap=self.max_line_gap,
            minLineLength=self.min_line_length
        )

    def _blur(self) -> np.ndarray:
        return cv.GaussianBlur(self._image, ksize=(self.gauss_ksize, self.gauss_ksize), sigmaX=0)

    def _canny(self) -> np.ndarray:
        return cv.Canny(self._blur(), threshold1=self.canny_threshold1, threshold2=self.canny_threshold2)

    def _draw_lines(self) -> tuple[np.ndarray, np.ndarray]:
        image = self._image.copy()

        edges, lines = self._hough_lines_p()
        if lines is None:
            return image

        for line in lines:
            x1, y1, x2, y2 = line[0]
            image = cv.line(image, (x1, y1), (x2, y2), color=(64, 64, 255), thickness=3)

        return edges, image

    def _parameter_changed(self) -> None:
        self._on_parameter_changed(*self._draw_lines())


def main() -> None:
    file_path = Path(easygui.fileopenbox(title='Open image') or '')

    if (image := cv.imread(str(file_path))) is None:
        return pymsgbox.alert(title='Error', text=f'Could not open file:\n{file_path}', icon=pymsgbox.WARNING)

    image = shrink_to_fit(image, (1200, 600))

    window_name = file_path.name
    cv.namedWindow(window_name, cv.WINDOW_GUI_EXPANDED)
    cv.resizeWindow(window_name, image.shape[1], image.shape[0])

    edges_window_name = f'{file_path.name} - edges'
    cv.namedWindow(edges_window_name, cv.WINDOW_GUI_EXPANDED)
    cv.resizeWindow(edges_window_name, image.shape[1], image.shape[0])

    def redraw(edges: np.ndarray, img: np.ndarray) -> None:
        cv.imshow(edges_window_name, edges)
        cv.imshow(window_name, img)

    model = Model(image, redraw)

    sliders_window_name = 'sliders'
    cv.namedWindow(sliders_window_name)
    cv.resizeWindow(sliders_window_name, 500, 400)

    cv.createTrackbar('gauss ksize', sliders_window_name, model.gauss_ksize, 33, model.set_gauss_ksize)
    cv.createTrackbar('canny threshold1', sliders_window_name, model.canny_threshold1, 255, model.set_canny_threshold1)
    cv.createTrackbar('canny threshold2', sliders_window_name, model.canny_threshold2, 255, model.set_canny_threshold2)
    cv.createTrackbar('rho', sliders_window_name, model.rho, 255, model.set_rho)
    cv.createTrackbar('theta', sliders_window_name, model.theta_degrees, 180, model.set_theta)
    cv.createTrackbar('threshold', sliders_window_name, model.threshold, 500, model.set_threshold)
    cv.createTrackbar('min line length', sliders_window_name, model.min_line_length, 500, model.set_min_line_length)
    cv.createTrackbar('max line gap', sliders_window_name, model.max_line_gap, 500, model.set_max_line_gap)

    cv.waitKey(0)


if __name__ == '__main__':
    main()
